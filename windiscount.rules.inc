<?php
/**
 * @file
 * Provides Rules module support for win discount module.
 */

/**
 * Implements hook_rules_action_info().
 */
function windiscount_rules_action_info() {
  $actions['windiscount_random_discount'] = array(
    'label' => t('Random discount'),
    'group' => t('Win discount'),
    'parameter' => array(
      'entity' => array(
        'label' => t('Entity'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
      'commerce_discount' => array(
        'label' => t('Commerce Discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
  );
  return $actions;
}

/**
 * Rules action callback: set product line item to specific amount.
 *
 * @param \EntityDrupalWrapper $wrapper
 *   The line item wrapper the fixed price discount would be applied to.
 * @param string $discount_name
 *   The discount name.
 */
function windiscount_random_discount(EntityDrupalWrapper $wrapper, $discount_name) {
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $discount_price_from = $discount_wrapper->commerce_discount_offer->commerce_offer_amount_from->value();
  $discount_price_to = $discount_wrapper->commerce_discount_offer->commerce_offer_amount_to->value();
  $discount_new_price_amount = mt_rand($discount_price_from['amount'], $discount_price_to['amount']);
  $discount_new_price = $discount_price_from;
  $discount_new_price['amount'] = - $discount_new_price_amount;

  // Check whether this discount was already added as a price component.
  $price_data = $wrapper->commerce_unit_price->data->value();
  foreach ($price_data['components'] as $component) {
    if (!empty($component['price']['data']['discount_name']) && $component['price']['data']['discount_name'] == $discount_wrapper->getIdentifier()) {
      return;
    }
  }

  // Figure out proper discount amount to apply in order to set proper price.
  $line_item_amount = $wrapper->commerce_unit_price->amount->value();

  // If the discount amount is more expensive than the line item's price, skip.
  if ($line_item_amount < $discount_new_price['amount']) {
    return;
  }

  module_load_include('inc', 'commerce_discount', 'commerce_discount.rules');

  //$discount_new_price['amount'] = $line_item_amount - $discount_new_price['amount'];

  commerce_discount_add_price_component($wrapper, $discount_name, $discount_new_price);
}